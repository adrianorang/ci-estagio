const expect = require('expect')
const calculadora = require('../src/calculadora.js')

describe('Calculadora Test - Soma', function() {

    it('Teste soma dois valores', function() {
        expect(calculadora.soma(2,3)).toEqual(5)
    })

    it('Teste soma n valores', function() {
        expect(calculadora.soma(2, 4, 3, 1)).toEqual(10)
    })

})
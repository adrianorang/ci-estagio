const soma = (...arrayValores) => {
    let aux = 0
    arrayValores.forEach(e => {
        aux = aux + e
    })

    return aux
}

module.exports = {
    soma
}